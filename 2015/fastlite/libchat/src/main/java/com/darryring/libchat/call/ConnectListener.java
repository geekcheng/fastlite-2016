package com.darryring.libchat.call;

/**
 * Created by hljdrl on 16/4/11.
 */
public interface ConnectListener {

    void OnConnect();

    void OnDisConnect(int what,String msg,Object obj);


}
