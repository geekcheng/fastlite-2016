package com.darryring.libchat.call;

/**
 * 消息监听
 * Created by user on 15/11/11.
 */
public interface MessageListener<T> {
    /**
     * 监听接收到消息
     */
    public void onReceiveMessge(T chatMessage);

    public void onMessageChanged();
}
