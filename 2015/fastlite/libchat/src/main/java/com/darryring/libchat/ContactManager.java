package com.darryring.libchat;

import java.util.List;

/**
 * Created by hljdrl on 16/4/11.
 */
public interface ContactManager<T> {

    /**
     *
     */
    void load();

    /**
     * @return
     */
    List<T> getContacts();
}
