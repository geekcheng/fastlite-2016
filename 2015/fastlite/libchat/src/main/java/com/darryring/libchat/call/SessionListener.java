package com.darryring.libchat.call;

/**
 * 回话管理器回调接口
 * Created by hljdrl on 15/11/19.
 */
public interface SessionListener<T> {

    void onCreateSession(T t);
    void onUpdateSession(T t);
    void onDeleteSession(T t);
}
