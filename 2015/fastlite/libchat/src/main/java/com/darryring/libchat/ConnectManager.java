package com.darryring.libchat;

import com.darryring.libchat.call.ConnectListener;

/**
 * Created by hljdrl on 16/4/11.
 */
public interface ConnectManager {
    void addConnectListener(ConnectListener _call);
    void removeConnectListener(ConnectListener _call);
}
