package com.darryring.libchat;

/**
 * 通知管理器,复制推送状态栏消息,播放音乐和震动
 * Created by hljdrl on 16/4/12.
 */
public interface NoticeManager {

    void init();
    /**
     * 是否开启震动提醒
     * @param _vibrate
     */
    void setVibrate(boolean _vibrate);

    /**
     * 是否开启铃声提醒
     * @param _bell
     */
    void setBell(boolean _bell);
    /**
     * 设置提醒数据
     * @param appName
     * @param iconId
     * @param action
     */
    void setNotifyLink(String appName,int iconId,String action);
    /**
     * 播放个人聊天声音提醒,前提聊天页面未打开
     * @param formName
     */
    void playChatMessage(String formName);
    /**
     * 播放群组聊天消息声音提醒,前提是聊天页面未打开
     * @param formName
     */
    void playGroupChatMessage(String formName);
    /**
     * 播放异常提示声音
     */
    void playError();

    /**
     * 清楚通知
     */
    void clearNotification();
}
