package com.darryring.libchat;

import com.darryring.libchat.call.OkCall;
import com.darryring.libchat.call.DataChageListener;

/**
 * Created by hljdrl on 16/4/11.
 */
public interface ChatClient<T> {


    /**
     * @param t
     */
    void init(T t);

    /**
     * @param k
     * @param v
     */
    void setConfig(String k,String v);

    /**
     * @param k
     * @return
     */
    String getConfig(String k);

    boolean isOnline();

    /**
     * @param user
     * @param pwd
     */
    void login(String user,String pwd);

    /**
     * @param user
     * @param pwd
     * @param _call
     */
    void loginAsyn(String user,String pwd,OkCall _call);
    /**
     *
     */
    void logout();

    void addDataChageListener(DataChageListener _listner);

    void removeDataChageListener(DataChageListener _listener);

    /**
     * @param _call
     */
    void logout(OkCall _call);

    /**
     * @return
     */
    ContactManager contactManager();

    /**
     * @return
     */
    ConnectManager connectManager();

    /**
     * @return
     */
    ChatManager chatManager();
}
