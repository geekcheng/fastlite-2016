package com.darryring.fast.fragment;

import android.annotation.SuppressLint;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.darryring.fast.R;
import com.darryring.fast.activitys.ChatActivity;
import com.darryring.fast.adapter.ContactAdapter;
import com.darryring.fast.util.DrawableUtil;
import com.darryring.libchat.ContactManager;
import com.darryring.libchat.LibChat;
import com.darryring.libchat.call.DataChageListener;
import com.darryring.libmodel.entity.FContact;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hljdrl on 16/3/2.
 */
public class ContactFragment extends BaseFragment implements AdapterView.OnItemClickListener,DataChageListener{


    ListView mListview;
    ContactManager<FContact> contactManager;
    private List<FContact> mContacts = new ArrayList<FContact>();
    ContactAdapter mContactAdapter;
    public ContactFragment(){
    }
    @SuppressLint("ValidFragment")
    public ContactFragment(String _title){
        setTitle(_title);
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(LibChat.chatClient!=null){
            contactManager = LibChat.chatClient.contactManager();
        }
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View _view = inflater.inflate(R.layout.fragment_contact,container,false);
        return _view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View _view = getView();
        mListview = (ListView) _view.findViewById(R.id.listview_contact);
        //=====================================================================
        TypedArray ta = getActivity().getTheme().obtainStyledAttributes(R.styleable.FastColorTheme);
        int _defaultColor = Color.TRANSPARENT;
        int _selectColor = ta.getColor(R.styleable.FastColorTheme_colorFastNomal,0);
        ColorStateList colorStateList = DrawableUtil.createTabBarColorStateList(_defaultColor,_selectColor);
        Drawable _drawable = mListview.getSelector();
        Drawable _tintDrawable =  DrawableUtil.tintDrawable(_drawable,colorStateList);
        mListview.setSelector(_tintDrawable);
        //----
        ta.recycle();

        //=====================================================================
        loadContacts();
        if(mContactAdapter==null){
            mContactAdapter = new ContactAdapter(getActivity(),mContacts);
            mListview.setAdapter(mContactAdapter);
        }else{
            mContactAdapter.notifyDataSetChanged();
        }
        mListview.setOnItemClickListener(this);
        if(LibChat.chatClient!=null){
            LibChat.chatClient.addDataChageListener(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(LibChat.chatClient!=null){
            LibChat.chatClient.removeDataChageListener(this);
        }
    }

    private void loadContacts(){
        mContacts.clear();
        if(contactManager!=null){
            List<FContact> _copy = contactManager.getContacts();
            if(_copy!=null){
                mContacts.addAll(_copy);
            }
            return ;
        }
        List<FContact> data = new ArrayList<FContact>();
        for(int i=0;i<100;i++) {
            FContact _ct = new FContact();
            _ct.setChatId(getFtName()+(i+1));
            data.add(_ct);
        }
        mContacts.clear();
        mContacts.addAll(mContacts);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        final FContact _select = mContacts.get(i);
        ChatActivity.runIntentChat(getActivity(),_select.getChatId(),_select.getChatId());
    }

    @Override
    public void sourceChange(Class<?> manager) {
        if(manager==ContactManager.class){
            if(getActivity()!=null && !getActivity().isFinishing()){
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                           loadContacts();
                           if(mContactAdapter!=null){
                               mContactAdapter.notifyDataSetChanged();
                           }
                    }
                });
            }
        }
    }
}
