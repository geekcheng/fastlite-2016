package com.darryring.fast.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.darryring.fast.R;
import com.darryring.fast.base.BaseAppCompatActivity;
import com.darryring.fast.theme.FastTheme;
import com.darryring.libcore.Fast;
import com.darryring.libmodel.entity.theme.ThemeEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * 二维码生成页面
 */
public class ThemeActivity extends BaseAppCompatActivity implements View.OnClickListener {

    private String TAG = "ThemeActivity";
    View viewStatusBar;
    View viewActionBar;
    List<View> itemViewButtons = new ArrayList<View>();
    LinearLayout themeLayout;
    ThemeEntity mThemeEntity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mThemeEntity = FastTheme.fastTheme.theme();
        FastTheme.fastTheme.setupTheme(this, ThemeEntity.TYPE_NO_ACTION_BAR);
        super.onCreate(savedInstanceState);
        Fast.logger.i(TAG, "onCreate....");
        setContentView(R.layout.activity_theme);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewStatusBar = findViewById(R.id.view_status_bar);
        viewActionBar = findViewById(R.id.view_action_bar);
        //
        themeLayout = (LinearLayout) findViewById(R.id.theme_layout);
        //
        View viewPanel1 = findViewById(R.id.item_theme_panel_1);
        View viewPanel2 = findViewById(R.id.item_theme_panel_2);
        View viewPanel3 = findViewById(R.id.item_theme_panel_3);
        View viewPanel4 = findViewById(R.id.item_theme_panel_4);
        addItemButton(viewPanel1);
        addItemButton(viewPanel2);
        addItemButton(viewPanel3);
        addItemButton(viewPanel4);
        //===========================================================================
        cacheThemeViewTag.clear();
        List<ThemeColor> _themelist = themeTags();
        ThemeViewTag _selectTheme = null;
        for(ThemeColor _themecolor:_themelist){
            FrameLayout _itemLayout = (FrameLayout) View.inflate(this, R.layout.item_theme_hs, null);
            //---------------------------------------
            View _panelView = _itemLayout.getChildAt(0);
            TextView _nameTheme = (TextView) _panelView.findViewById(R.id.tv_theme_name);
            CheckBox _checkBox = (CheckBox) _panelView.findViewById(R.id.checkbox_theme);
            ThemeViewTag _cacheTag = new ThemeViewTag();
            _cacheTag.checkBox = _checkBox;
            _cacheTag.panelView = _panelView;
            _cacheTag.themeColor = _themecolor;
            _nameTheme.setText(_themecolor.name);
            _panelView.setBackgroundColor(_themecolor.colorFastNomal);
            _panelView.setTag(_cacheTag);
            cacheThemeViewTag.add(_cacheTag);
            //---------------------------------------
            themeLayout.addView(_itemLayout);
            if(mThemeEntity!=null&&_themecolor.themeEntity!=null){
                if(mThemeEntity.getTheme()==_themecolor.themeEntity.getTheme()){
                    _selectTheme = _cacheTag;
                }
            }
        }
        if(_selectTheme!=null) {
            changeTheme(_selectTheme);
        }

    }
    List<ThemeViewTag> cacheThemeViewTag = new ArrayList<ThemeViewTag>();
    class ThemeViewTag{
        public ThemeColor themeColor;
        public View panelView;
        public CheckBox checkBox;
    }
    List<ThemeColor> themeTags(){
        List<ThemeColor> _list = new ArrayList<ThemeColor>();
        //-----------------------------------------------------
        ThemeColor _hellokitty = new ThemeColor();
        _hellokitty.colorFastNomal = getResources().getColor(R.color.theme_color_hellokitty);
        _hellokitty.colorFastPressed =getResources().getColor( R.color.theme_color_hellokitty_pressed);
        _hellokitty.themeEntity = createThemeEntity(R.style.Theme_Splash,R.style.Hellokitty,R.style.Hellokitty_NoActionBar
        ,R.style.Hellokitty_Friends_NoActionBar,R.style.Hellokitty_Browsers_NoActionBar);
        _hellokitty.name="HelloKitty";
        _list.add(_hellokitty);
        //-----------------------------------------------------
        ThemeColor _amethyst = new ThemeColor();
        _amethyst.colorFastNomal = getResources().getColor(R.color.theme_color_AMETHYST);
        _amethyst.colorFastPressed = getResources().getColor(R.color.theme_color_AMETHYST_pressed);
        _amethyst.themeEntity = createThemeEntity(R.style.Theme_Splash,R.style.Amethyst,R.style.Amethyst_NoActionBar
                ,R.style.Amethyst_Friends_NoActionBar,R.style.Amethyst_Browsers_NoActionBar);
        _amethyst.name="AMETHYST";
        _list.add(_amethyst);
        //-----------------------------------------------------
        ThemeColor mTURQUOISE = new ThemeColor();
        mTURQUOISE.colorFastNomal = getResources().getColor(R.color.theme_color_TURQUOISE);
        mTURQUOISE.colorFastPressed = getResources().getColor(R.color.theme_color_TURQUOISE_pressed);
        mTURQUOISE.themeEntity = createThemeEntity(R.style.Theme_Splash,R.style.TURQUOISE,R.style.TURQUOISE_NoActionBar
                ,R.style.TURQUOISE_Friends_NoActionBar,R.style.TURQUOISE_Browsers_NoActionBar);
        mTURQUOISE.name="TURQUOISE";
        _list.add(mTURQUOISE);
        //-----------------------------------------------------
        ThemeColor mEMERALD = new ThemeColor();
        mEMERALD.colorFastNomal = getResources().getColor(R.color.theme_color_EMERALD);
        mEMERALD.colorFastPressed = getResources().getColor(R.color.theme_color_EMERALD_pressed);
        mEMERALD.name="EMERALD";
        mEMERALD.themeEntity =   createThemeEntity(R.style.Theme_Splash,R.style.EMERALD,R.style.EMERALD_NoActionBar
                ,R.style.EMERALD_Friends_NoActionBar,R.style.EMERALD_Browsers_NoActionBar);
        _list.add(mEMERALD);
        //-----------------------------------------------------
        ThemeColor mPETERRIVER = new ThemeColor();
        mPETERRIVER.colorFastNomal = getResources().getColor(R.color.theme_color_PETERRIVER);
        mPETERRIVER.colorFastPressed = getResources().getColor(R.color.theme_color_PETERRIVER_pressed);
        mPETERRIVER.name="PETERRIVER";
        mPETERRIVER.themeEntity  = createThemeEntity(R.style.Theme_Splash,R.style.PETERRIVER,
                R.style.PETERRIVER_NoActionBar,R.style.PETERRIVER_Friends_NoActionBar,
                R.style.PETERRIVER_Browsers_NoActionBar);
        _list.add(mPETERRIVER);
        //-----------------------------------------------------
        ThemeColor mWETASPHALT = new ThemeColor();
        mWETASPHALT.colorFastNomal = getResources().getColor(R.color.theme_color_WETASPHALT);
        mWETASPHALT.colorFastPressed = getResources().getColor(R.color.theme_color_WETASPHALT_pressed);
        mWETASPHALT.name="WETASPHALT";
        mWETASPHALT.themeEntity  = createThemeEntity(R.style.Theme_Splash,R.style.WETASPHALT,
                R.style.WETASPHALT_NoActionBar,R.style.WETASPHALT_Friends_NoActionBar,
                R.style.WETASPHALT_Browsers_NoActionBar);
        _list.add(mWETASPHALT);
        //-----------------------------------------------------
        ThemeColor mSUNFLOWER = new ThemeColor();
        mSUNFLOWER.colorFastNomal = getResources().getColor(R.color.theme_color_SUNFLOWER);
        mSUNFLOWER.colorFastPressed = getResources().getColor(R.color.theme_color_SUNFLOWER_pressed);
        mSUNFLOWER.name="SUNFLOWER";
        mSUNFLOWER.themeEntity =  createThemeEntity(R.style.Theme_Splash,R.style.SUNFLOWER,
                R.style.SUNFLOWER_NoActionBar,R.style.SUNFLOWER_Friends_NoActionBar,
                R.style.SUNFLOWER_Browsers_NoActionBar);
        _list.add(mSUNFLOWER);
        //-----------------------------------------------------
        ThemeColor mCARROT = new ThemeColor();
        mCARROT.colorFastNomal = getResources().getColor(R.color.theme_color_CARROT);
        mCARROT.colorFastPressed = getResources().getColor(R.color.theme_color_CARROT_pressed);
        mCARROT.name="CARROT";
        mCARROT.themeEntity =    createThemeEntity(R.style.Theme_Splash,R.style.CARROT,
                R.style.CARROT_NoActionBar,R.style.CARROT_Friends_NoActionBar,
                R.style.CARROT_Browsers_NoActionBar);
        _list.add(mCARROT);
        //-----------------------------------------------------
        ThemeColor mALIZARIN= new ThemeColor();
        mALIZARIN.colorFastNomal = getResources().getColor(R.color.theme_color_ALIZARIN);
        mALIZARIN.colorFastPressed = getResources().getColor(R.color.theme_color_ALIZARIN_pressed);
        mALIZARIN.name="ALIZARIN";
        mALIZARIN.themeEntity =   createThemeEntity(R.style.Theme_Splash,R.style.ALIZARIN,
                R.style.ALIZARIN_NoActionBar,R.style.ALIZARIN_Friends_NoActionBar,
                R.style.ALIZARIN_Browsers_NoActionBar);
        _list.add(mALIZARIN);
        //-----------------------------------------------------
        ThemeColor mCLOUDS = new ThemeColor();
        mCLOUDS.colorFastNomal = getResources().getColor(R.color.theme_color_CLOUDS);
        mCLOUDS.colorFastPressed = getResources().getColor(R.color.theme_color_CLOUDS_pressed);
        mCLOUDS.name="CLOUDS";
        mCLOUDS.themeEntity = createThemeEntity(R.style.Theme_Splash,R.style.CLOUDS,
                R.style.CLOUDS_NoActionBar,R.style.CLOUDS_Friends_NoActionBar,
                R.style.CLOUDS_Browsers_NoActionBar);
        _list.add(mCLOUDS);
        //-----------------------------------------------------
        ThemeColor mCONCRETE = new ThemeColor();
        mCONCRETE.colorFastNomal = getResources().getColor(R.color.theme_color_CONCRETE);
        mCONCRETE.colorFastPressed = getResources().getColor(R.color.theme_color_CONCRETE_pressed);
        mCONCRETE.name="CONCRETE";
        mCONCRETE.themeEntity =  createThemeEntity(R.style.Theme_Splash,R.style.CONCRETE,
                R.style.CONCRETE_NoActionBar,R.style.CONCRETE_Friends_NoActionBar,
                R.style.CONCRETE_Browsers_NoActionBar);
        _list.add(mCONCRETE);
        //-----------------------------------------------------
        return _list;
    }

    /**
     * @param SplashTheme
     * @param theme
     * @param NoActionBarTheme
     * @param FriendsNoActionBarTheme
     * @param BrowsersNoActionBarTheme
     * @return
     */
    ThemeEntity createThemeEntity(int SplashTheme,int theme,int NoActionBarTheme,int FriendsNoActionBarTheme,
                                  int BrowsersNoActionBarTheme){
        ThemeEntity entity = new ThemeEntity();
        entity.setTheme(theme);
        entity.setSplashTheme(SplashTheme);
        entity.setNoActionBarTheme(NoActionBarTheme);
        entity.setFriendsNoActionBarTheme(FriendsNoActionBarTheme);
        entity.setBrowsersNoActionBarTheme(BrowsersNoActionBarTheme);
        return entity;
    }

    void changeTheme(ThemeViewTag _viewTag){
        ThemeColor _themeColor = _viewTag.themeColor;
        viewStatusBar.setBackgroundColor(_themeColor.colorFastPressed);
        viewActionBar.setBackgroundColor(_themeColor.colorFastNomal);
        for(View _view:itemViewButtons){
            _view.setBackgroundColor(_themeColor.colorFastNomal);
        }

        List<ThemeViewTag> _caches = new ArrayList<ThemeViewTag>(cacheThemeViewTag);
        for(ThemeViewTag _tag:_caches){
            if(_tag==_viewTag){
                if(_tag.checkBox.getVisibility()!=View.VISIBLE){
                    _tag.checkBox.setVisibility(View.VISIBLE);
                    _tag.checkBox.setChecked(true);
                }
            }else{
                if(_tag.checkBox.getVisibility()!=View.INVISIBLE){
                    _tag.checkBox.setVisibility(View.INVISIBLE);
                    _tag.checkBox.setChecked(false);
                }
            }
        }
        if(_themeColor.themeEntity!=null){
            FastTheme.fastTheme.setApplyTheme(_themeColor.themeEntity);
        }
    }

    void addItemButton(View _view) {
        if (_view != null) {
            View _btnView = _view.findViewById(R.id.item_button);
            if (_btnView != null) {
                itemViewButtons.add(_btnView);
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode==KeyEvent.KEYCODE_BACK){
            Intent intent = new  Intent(this,HomeActivity.class);
            overridePendingTransition(0, 0);//不设置进入退出动画
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            overridePendingTransition(0, 0);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent intent = new  Intent(this,HomeActivity.class);
            overridePendingTransition(0, 0);//不设置进入退出动画
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            overridePendingTransition(0, 0);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Fast.logger.i(TAG, "onPause....");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Fast.logger.i(TAG, "onDestroy....");
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.item_theme_panel) {
            ThemeViewTag resId = (ThemeViewTag) v.getTag();
            changeTheme(resId);
        }
    }

    private class ThemeColor{
        public ThemeEntity themeEntity;
        public String name;
        public int colorFastNomal;
        public int colorFastPressed;
    }
}
