package com.darryring.fast.activitys;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.darryring.fast.R;

/**
 * Created by hljdrl on 16/5/31.
 */
public class LoadResActivity extends Activity {

    String TAG="LoadResActivity";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super .onCreate(savedInstanceState);
        Log.d(TAG, "onCreate" );
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN , WindowManager.LayoutParams.FLAG_FULLSCREEN );
        overridePendingTransition(R.anim.null_anim, R.anim.null_anim);
        setContentView(R.layout.layout_load);
//        new LoadDexTask().execute();
    }
//    class LoadDexTask extends AsyncTask {
//        @Override
//        protected Object doInBackground(Object[] params) {
//            try {
//                MultiDex.install(getApplication());
//                Log.d(TAG , "loadDex install finish" );
//                ((DarryApplication) getApplication()).installFinish(getApplication());
//            } catch (Exception e) {
//                Log.e(TAG , e.getLocalizedMessage());
//            }
//            return null;
//        }
//        @Override
//        protected void onPostExecute(Object o) {
//            Log.d( "loadDex", "get install finish");
//            finish();
//            System.exit( 0);
//        }
//    }
    @Override
    public void onBackPressed() {
        //cannot backpress
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
