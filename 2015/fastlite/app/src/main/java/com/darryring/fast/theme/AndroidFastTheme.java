package com.darryring.fast.theme;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.alibaba.fastjson.JSON;
import com.darryring.fast.R;
import com.darryring.libcore.Fast;
import com.darryring.libcore.util.StringUtil;
import com.darryring.libmodel.entity.theme.ThemeEntity;

/**
 * Created by hljdrl on 16/6/4.
 */
public class AndroidFastTheme extends FastTheme{
    String TAG="AndroidFastTheme";
    String VERSION="1.0.0";
    Context mContext;
    private ThemeEntity themeEntity;
    public AndroidFastTheme(){
        //--------------------------------------------------------------------
//        ThemeEntity _defaultTheme = new ThemeEntity();
//        _defaultTheme.setThemeId("1");
//        _defaultTheme.setSplashTheme(R.style.Theme_Splash);
//        _defaultTheme.setTheme(R.style.Amethyst);
//        _defaultTheme.setNoActionBarTheme(R.style.Amethyst_NoActionBar);
//        _defaultTheme.setFriendsNoActionBarTheme(R.style.Amethyst_Friends_NoActionBar);
//        _defaultTheme.setBrowsersNoActionBarTheme(R.style.Amethyst_Browsers_NoActionBar);
//        themeEntity = _defaultTheme;
    }
    void saveThemeConfig(){
        sp.edit().putString("theme", JSON.toJSONString(themeEntity)).commit();
    }
    ThemeEntity createDefaultTheme(){
        ThemeEntity _defaultTheme = new ThemeEntity();
        _defaultTheme.setThemeId("1");
        _defaultTheme.setSplashTheme(R.style.Theme_Splash);
        _defaultTheme.setTheme(R.style.Amethyst);
        _defaultTheme.setNoActionBarTheme(R.style.Amethyst_NoActionBar);
        _defaultTheme.setFriendsNoActionBarTheme(R.style.Amethyst_Friends_NoActionBar);
        _defaultTheme.setBrowsersNoActionBarTheme(R.style.Amethyst_Browsers_NoActionBar);
        return _defaultTheme;
    }

    SharedPreferences sp;
    @Override
    public void init(Activity context) {
        mContext = context.getApplicationContext();
        sp = mContext.getSharedPreferences("ThemeConfig",Context.MODE_MULTI_PROCESS);
        if(Fast.logger!=null){
            Fast.logger.i(TAG,"--->init...");
            Fast.logger.i(TAG,"--->VERSION:"+VERSION);
        }
        String _fileTheme = sp.getString("theme","");
        if(StringUtil.isNotNull(_fileTheme)){
            ThemeEntity _entity = JSON.parseObject(_fileTheme,ThemeEntity.class);
            if(_entity!=null){
                themeEntity = _entity;
            }
        }else {
            themeEntity = createDefaultTheme();
        }
    }

    @Override
    public void setApplyTheme(ThemeEntity _themeEntity) {
        if(_themeEntity!=null) {
            if(themeEntity!=null){
                if(_themeEntity.getTheme()!=themeEntity.getTheme()){
                    themeEntity = _themeEntity;
                    saveThemeConfig();
                }
            }else{
                themeEntity = _themeEntity;
                saveThemeConfig();
            }

        }
    }
    @Override
    public void setupTheme(Activity activity, int themeType) {
        int _theme = 0;
        switch(themeType){
            case ThemeEntity.TYPE_THEME:
                _theme = themeEntity.getTheme();
                break;
            case ThemeEntity.TYPE_SPLASH_THEME:
                _theme = themeEntity.getSplashTheme();
                break;
            case ThemeEntity.TYPE_NO_ACTION_BAR:
                _theme = themeEntity.getNoActionBarTheme();
                break;
            case ThemeEntity.TYPE_FRIENDS_NO_ACTION_BAR_THEME:
                _theme = themeEntity.getFriendsNoActionBarTheme();
                break;
            case ThemeEntity.TYPE_BROWSER_NOACTION_BAR:
                _theme = themeEntity.getBrowsersNoActionBarTheme();
                break;
        }
        activity.setTheme(_theme);
    }

    @Override
    public ThemeEntity theme() {
        return themeEntity;
    }
}
