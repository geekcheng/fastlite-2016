package com.darryring.fast.util;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageManager;

import java.util.List;

/**
 * Created by hljdrl on 16/4/11.
 */
public class ApkUtil {

    /**
     * @param pID
     * @param context
     * @return
     */
    public static final  String getAppName(int pID, Context context) {
        String processName = null;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> list = am.getRunningAppProcesses();
        PackageManager pm = context.getPackageManager();
        if(list!=null) {
            for (ActivityManager.RunningAppProcessInfo _info : list) {
                try {
                    if (_info.pid == pID) {
                        CharSequence c = pm
                                .getApplicationLabel(pm.getApplicationInfo(_info.processName, PackageManager.GET_META_DATA));
                        processName = _info.processName;
                        return processName;
                    }
                } catch (Exception e) {
                }
            }
        }
        return processName;
    }
}
