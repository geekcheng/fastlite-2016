package com.darryring.fast.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;

import com.darryring.libcore.Fast;

/**
 * Created by hljdrl on 16/3/2.
 */
public  class BaseFragment  extends Fragment{

    private String mFtName = getClass().getSimpleName();

    public String getFtName() {
        return mFtName;
    }

    public void setFtName(String mFtName) {
        this.mFtName = mFtName;
    }

    private String title="";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fast.logger.i(mFtName,"onCreate()....");
    }

    @Override
    public void onResume() {
        super.onResume();
        Fast.logger.i(mFtName,"onResume()....");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Fast.logger.i(mFtName,"onCreateView()....");
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        Fast.logger.i(mFtName,"onViewCreated()....");
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        Fast.logger.i(mFtName,"onCreateAnimation()....");
        return super.onCreateAnimation(transit, enter, nextAnim);
    }

    @Override
    public void onPause() {
        Fast.logger.i(mFtName,"onPause()....");
        super.onPause();
    }

    @Override
    public void onDestroy() {
        Fast.logger.i(mFtName,"onDestroy()....");
        super.onDestroy();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Fast.logger.i(mFtName,"onActivityCreated()....");
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Fast.logger.i(mFtName,"onActivityResult()....");
        super.onActivityResult(requestCode, resultCode, data);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
