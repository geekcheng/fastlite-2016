package com.darryring.libcore.task;


/**
 * Created by hljdrl on 15/12/11.
 */
public interface Task {

    /**
     * @param _run
     */
    public void postTask(Runnable _run);

    /**
     * @param _run
     */
    public void postUiTask(Runnable _run);

    /**
     * @param _run
     * @param delayed
     */
    public void postUiTaskAsDelayed(Runnable _run,long delayed);

    /**
     *
     */
    public void shutdownTask();
}
