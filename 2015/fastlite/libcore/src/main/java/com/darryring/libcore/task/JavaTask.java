package com.darryring.libcore.task;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by hljdrl on 15/12/25.
 */
public class JavaTask implements  Task {

    ExecutorService fixedThreadPool;
    public JavaTask()
    {
        fixedThreadPool = Executors.newFixedThreadPool(8,new BackgroundThreadFactory());
    }
    /**
     * @param _run
     */
    @Override
    public void postTask(Runnable _run) {
        fixedThreadPool.execute(_run);
    }

    /**
     * @param _run
     */
    @Override
    public void postUiTask(Runnable _run) {
        fixedThreadPool.execute(_run);
    }

    /**
     * @param _run
     * @param delayed
     */
    @Override
    public void postUiTaskAsDelayed(Runnable _run, long delayed) {

    }

    /**
     *
     */
    @Override
    public void shutdownTask() {
        fixedThreadPool.shutdown();
    }

}
